import django_filters
from project_library.const import com_filter
from project_library.models import Company


class CompanyFilter(django_filters.FilterSet):
    ordering = django_filters.OrderingFilter(choices=com_filter, required=True)

    class Meta:
        model = Company
        fields = ['name']
        order_by = 'name'

