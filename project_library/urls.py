from django.urls import path, include
from . import views_com, views_pro, views_mes, views

urlpatterns = [
    path('account/create/', views.UserSignUpView.as_view(), name='signup'),
    path('account/login/', views.UserLoginView.as_view(), name='login'),
    path('', views_com.CompanyListView.as_view(), name='home'),
    path('company/', include([
        path('all/', views_com.CompanyListView.as_view(), name='com_all'),
        path('add/', views_com.CompanyCreateView.as_view(), name='com_add'),
        path('<slug:slug>/edit/', views_com.CompanyEditView.as_view(), name='com_edit'),
        path('<slug:slug>/', views_com.CompanyDetailView.as_view(), name='com_detail'),
    ],), name='company'),
    path('project/', include([
        path('all/', views_pro.ProjectListView.as_view(), name='pro_all'),
        path('add/',  views_pro.ProjectCreateView.as_view(), name='pro_add'),
        path('<slug:slug>/edit',  views_pro.ProjectEditView.as_view(), name='pro_edit'),
        path('<slug:slug>/', views_pro.ProjectDetailView.as_view(), name='pro_detail'),
    ],), name='project'),
    path('massage/', include([
        path('all/', views_mes.MessageListView.as_view(), name='mes_all'),
        path('add/', views_mes.MessageCreateView.as_view(), name='mes_add'),
        path('<slug:slug>/edit/', views_mes.MessageCreateView.as_view(), name='mes_edit'),
        path('<slug:slug>/', views_mes.MessageDetailView.as_view(), name='mes_detail'),
    ],), name='message'),

]
