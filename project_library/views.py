from django.contrib.auth.views import LoginView
from django.urls import reverse_lazy
from django.views.generic import CreateView
from crm.forms import SignUpForm, LoginForm


class UserSignUpView(CreateView):
    form_class = SignUpForm
    success_url = reverse_lazy('login')
    template_name = 'signup.html'


class UserLoginView(LoginView):
    form_class = LoginForm
    template_name = 'login.html'
