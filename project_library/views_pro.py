from django.views.generic import ListView, CreateView, DetailView, UpdateView
from crm.forms import ProjectForm
from project_library.mixins import ProjectCreateEditMixin
from project_library.models import Project


class ProjectListView(ListView):
    paginate_by = 5
    model = Project
    context_object_name = 'projects'
    template_name = 'pro_all.html'


class ProjectDetailView(DetailView):
    model = Project
    context_object_name = 'projects_detail'
    template_name = 'pro_detail.html'
    queryset = Project.objects.all().prefetch_related('projects_massage')


class ProjectCreateView(ProjectCreateEditMixin, CreateView):
    model = Project
    form_class = ProjectForm
    template_name = 'pro_form.html'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['title'] = 'Добавить проект'
        return context


class ProjectEditView(ProjectCreateEditMixin, UpdateView):
    model = Project
    form_class = ProjectForm
    template_name = 'pro_form.html'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['title'] = 'Изменить проект'
        context['update'] = True
        return context
