from django.views.generic import ListView, CreateView, DetailView
from crm.forms import MessageForm
from project_library.models import Message


class MessageListView(ListView):
    paginate_by = 8
    model = Message
    context_object_name = 'messages'
    template_name = 'mes_all.html'



class MessageDetailView(DetailView):
    model = Message
    context_object_name = 'messages_detail'
    template_name = 'mes_detail.html'
#queryset = Message.objects.all().prefetch_related('message_users')


class MessageCreateView(CreateView):
    model = Message
    form_class = MessageForm
    template_name = 'mes_form.html'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['title'] = 'Добавить сообщение'
        return context




