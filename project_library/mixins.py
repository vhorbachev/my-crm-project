from crm.forms import CompanyPhoneInlineFormset, CompanyEmailInlineFormset, MessageUsersInlineFormset


class CompanyCreateEditMixin:
    def form_valid(self, form):
        context = self.get_context_data(form=form)
        formsets = [context['companyEmailInlineFormset'], context['companyPhoneInlineFormset']]
        for formset in formsets:
            if formset.is_valid() and form.is_valid():
                instances = formset.save(commit=False)
                for instance in instances:
                    if self.object is not None:
                        instance.company = self.object
                        instance.save()
                    else:
                        super().form_valid(form)
            else:
                return super().form_invalid(form)
        return super().form_valid(form)

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        if self.request.POST:
            context['companyEmailInlineFormset'] = CompanyEmailInlineFormset(
                self.request.POST, instance=self.object)
            context['companyEmailInlineFormset'].full_clean()
            context['companyPhoneInlineFormset'] = CompanyPhoneInlineFormset(
                self.request.POST, instance=self.object)
            context['companyPhoneInlineFormset'].full_clean()
        else:
            context['companyEmailInlineFormset'] = CompanyEmailInlineFormset(
                instance=self.object)
            context['companyPhoneInlineFormset'] = CompanyPhoneInlineFormset(
                instance=self.object)
        return context


class ProjectCreateEditMixin:
    def form_valid(self, form):
        if form.is_valid():
            instance = form.save(commit=False)
            instance.save()
        else:
            return super().form_invalid(form)
        return super().form_valid(form)


class MessageCreateEditMixin:
    def form_valid(self, form):
        if form.is_valid():
            instance = form.save(commit=False)
            instance.save()
        else:
            return super().form_invalid(form)
        return super().form_valid(form)


class MessageCreateEditMixin:
    def form_valid(self, form):
        context = self.get_context_data(form=form)
        formsets = [context['messageUsersInlineFormset']]
        for formset in formsets:
            if formset.is_valid() and form.is_valid():
                instances = formset.save(commit=False)
                for instance in instances:
                    if self.object is not None:
                        instance.message = self.object
                        instance.save()
                    else:
                        super().form_valid(form)
            else:
                return super().form_invalid(form)
        return super().form_valid(form)

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        if self.request.POST:
            context['messageUsersInlineFormset'] = MessageUsersInlineFormset(
                self.request.POST, instance=self.object)
            context['massageUsersInlineFormset'].full_clean()
        else:
            context['messageUsersInlineFormset'] = MessageUsersInlineFormset(
                instance=self.object)
        return context
