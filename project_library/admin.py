from django.contrib import admin
from crm.forms import ProjectAdminForm, CompanyAdminForm, MessageAdminForm
from .models import Company, Project, Email, Phone, Message


@admin.register(Project)
class ProjectAdmin(admin.ModelAdmin):
    list_display = ('name', 'company', 'description', 'date_begin', 'date_end', 'price', 'is_active')
    form = ProjectAdminForm


@admin.register(Email)
class EmailAdmin(admin.ModelAdmin):
    list_display = ('company', 'email', 'created_at', 'updated_at')
    fields = ['company', 'email', 'created_at', 'updated_at']


@admin.register(Phone)
class PhoneAdmin(admin.ModelAdmin):
    list_display = ('company', 'phone', 'created_at', 'updated_at')
    fields = ['company', 'phone', 'created_at', 'updated_at']


class PhoneInLine(admin.TabularInline):
    model = Phone
    extra = 0


class EmailInLine(admin.TabularInline):
    model = Email
    extra = 0


@admin.register(Company)
class CompanyAdmin(admin.ModelAdmin):
    list_display = ('name', 'leader', 'info', 'address', 'is_active')
    fields = ['name', 'slug', 'leader', 'info', 'address', 'is_active']
    inlines = [EmailInLine, PhoneInLine]
    form = CompanyAdminForm


@admin.register(Message)
class MessageAdmin(admin.ModelAdmin):
    list_display = ('project', 'tittle', 'type', 'text', 'created_at', 'updated_at')
    fields = ['project', 'tittle', 'slug', 'type', 'text']
    form = MessageAdminForm

