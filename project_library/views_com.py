from django.views.generic import CreateView, DetailView, UpdateView
from django_filters.views import FilterView
from crm.forms import CompanyForm
from project_library.filters import CompanyFilter
from project_library.mixins import CompanyCreateEditMixin
from project_library.models import Company


class CompanyListView(FilterView):
    paginate_by = 2
    model = Company
    filterset_class = CompanyFilter
    context_object_name = 'companies'
    template_name = 'com_all.html'
    queryset = Company.objects.all().prefetch_related('companies_emails', 'companies_phones')


class CompanyDetailView(DetailView):
    model = Company
    context_object_name = 'companies_detail'
    template_name = 'com_detail.html'
    queryset = Company.objects.all().prefetch_related('companies_emails', 'companies_projects',
                                                      'companies_phones')


class CompanyCreateView(CompanyCreateEditMixin, CreateView):
    model = Company
    form_class = CompanyForm
    template_name = 'com_form.html'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['title'] = 'Добавить компанию'
        return context


class CompanyEditView(CompanyCreateEditMixin, UpdateView):
    model = Company
    form_class = CompanyForm
    template_name = 'com_form.html'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['title'] = 'Изменить компанию'
        context['update'] = True
        return context
