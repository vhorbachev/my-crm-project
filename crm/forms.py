from ckeditor_uploader.widgets import CKEditorUploadingWidget
from django import forms
from django.contrib.auth import get_user_model
from django.contrib.auth.forms import UsernameField, AuthenticationForm
from django.core.exceptions import ValidationError
from django.forms import inlineformset_factory
from project_library.models import Company, Email, Phone, Project, Message, Users


class CompanyForm(forms.ModelForm):
    info = forms.CharField(widget=CKEditorUploadingWidget(), label='Описание')

    class Meta:
        model = Company
        fields = ('name', 'leader', 'info', 'address', 'is_active')
        widgets = {'name': forms.TextInput(
                       attrs={'class': 'form-control mb-2', 'placeholder': 'Название компании'}),
                   'leader': forms.TextInput(
                       attrs={'class': 'form-control mb-2', 'placeholder': 'Контактное лицо (ФИО)'}),
                   'address': forms.TextInput(
                       attrs={'class': 'form-control mb-2', 'placeholder': 'Адресс'}),
        }


class EmailForm(forms.ModelForm):
    class Meta:
        model = Email
        fields = ['email']


class PhoneForm(forms.ModelForm):
    class Meta:
        model = Phone
        fields = ['phone']


class ProjectForm(forms.ModelForm):
    description = forms.CharField(widget=CKEditorUploadingWidget(), label='Описание')

    class Meta:
        model = Project
        fields = ['name', 'description', 'date_begin', 'date_end', 'price', 'is_active', 'company']
        widgets = {'name': forms.TextInput(
            attrs={'class': 'form-control mb-2', 'placeholder': 'Название проекта'}),
                    'description': forms.TextInput(
            attrs={'class': 'form-control mb-2', 'placeholder': 'Описание'}),
                    'date_begin': forms.DateInput(
                attrs={'class': 'form-control mb-2'}),
                    'date_end': forms.DateInput(
                attrs={'class': 'form-control mb-2'}),
                    'price': forms.TextInput(
                attrs={'class': 'form-control mb-2'}),
        }


class UsersForm(forms.ModelForm):
    class Meta:
        model = Users
        fields = ['username']


CompanyPhoneInlineFormset = inlineformset_factory(
    Company,
    Phone,
    form=PhoneForm,
    can_delete=False,
    extra=5,)


CompanyEmailInlineFormset = inlineformset_factory(
    Company,
    Email,
    form=EmailForm,
    can_delete=False,
    extra=5,)


MessageUsersInlineFormset = inlineformset_factory(
    Message,
    Users,
    form=UsersForm,
    can_delete=False)


class MessageForm(forms.ModelForm):
    text = forms.CharField(widget=CKEditorUploadingWidget(), label='Описание')

    class Meta:
        model = Message
        fields = ['tittle', 'project', 'type', 'text']
        widgets = {'tittle': forms.TextInput(
            attrs={'class': 'form-control mb-2', 'placeholder': 'Teмa'}),
                    'text': forms.TextInput(
                attrs={'class': 'form-control mb-2', 'placeholder': 'Текст'}),
        }


class CompanyAdminForm(forms.ModelForm):
    info = forms.CharField(widget=CKEditorUploadingWidget(), label='Описание')

    class Meta:
        model = Company
        fields ='__all__'


class ProjectAdminForm(forms.ModelForm):
    description = forms.CharField(widget=CKEditorUploadingWidget(), label='Описание')

    class Meta:
        model = Project
        fields ='__all__'


class MessageAdminForm(forms.ModelForm):
    text = forms.CharField(widget=CKEditorUploadingWidget(), label='Текст')

    class Meta:
        model = Message
        fields = '__all__'


class SignUpForm(forms.ModelForm):

    email = UsernameField(label='Введите e-mail пользователя', widget=forms.TextInput(
        attrs={'class': 'form-control mb-2'}))
    password1 = forms.CharField(strip=False, label="Введите пароль", widget=forms.PasswordInput(
        attrs={'class': 'form-control mb-2', 'autocomplete': 'new-password'}))
    password2 = forms.CharField(strip=False, label="Подтвердите пароль", widget=forms.PasswordInput(
        attrs={'class': 'form-control mb-2', 'autocomplete': 'new-password'}))

    class Meta:
        model = get_user_model()
        fields = ['email', 'password1', 'password2']

    def clean_password2(self):
        pass1 = self.cleaned_data.get("password1")
        pass2 = self.cleaned_data.get("password2")
        if pass1 and pass2 and pass1 != pass2:
            raise ValidationError('Пароли не совпадают!')
        return pass2

    def save(self, commit=True):
        return get_user_model().create_user(self.cleaned_data['email'], self.cleaned_data['password1'])


class LoginForm(AuthenticationForm):
    username = UsernameField(label='Введите e-mail пользователя', widget=forms.TextInput(
        attrs={'class': 'form-control mb-2'}))
    password = forms.CharField(strip=False, label="Введите пароль", widget=forms.PasswordInput(
        attrs={'class': 'form-control mb-2', 'autocomplete': 'new-password'}))
